using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;


public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public string playerName;
    public TMP_InputField nameField;
    public string hiscorePlayerName;
    public int m_HighPoints = 0;
    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
           
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        LoadPoints();
        

    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    [System.Serializable]
    class SaveData
    {
        public string playerName;
        public string hiscorePlayerName;
        public int m_HighPoints = 0;
    }
    public void SavePoints()
    {
        SaveData data = new SaveData();
        data.playerName = playerName;
        data.hiscorePlayerName = playerName;
        data.m_HighPoints = m_HighPoints;

        string json = JsonUtility.ToJson(data);

        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);

    }
    public void LoadPoints()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            SaveData data = JsonUtility.FromJson<SaveData>(json);
            playerName = data.playerName;
            hiscorePlayerName = data.playerName;
            m_HighPoints = data.m_HighPoints;
        }
    }
}
